#pragma once

#include "openfhe/core/lattice/hal/lat-backend.h"
#include "openfhe/pke/key/privatekey-fwd.h"

#include "SerialMode.h" // SerialMode

namespace openfhe
{

using PrivateKeyImpl = lbcrypto::PrivateKeyImpl<lbcrypto::DCRTPoly>;

class PrivateKeyDCRTPoly final
{
    std::shared_ptr<PrivateKeyImpl> m_privateKey;
public:
    friend bool SerializePrivateKeyToFile(const std::string& privateKeyLocation,
        const PrivateKeyDCRTPoly& privateKey, const SerialMode serialMode);
    friend bool DeserializePrivateKeyFromFile(const std::string& privateKeyLocation,
        PrivateKeyDCRTPoly& privateKey, const SerialMode serialMode);

    PrivateKeyDCRTPoly() = default;
    explicit PrivateKeyDCRTPoly(const std::shared_ptr<PrivateKeyImpl>& privateKey);
    PrivateKeyDCRTPoly(const PrivateKeyDCRTPoly&) = delete;
    PrivateKeyDCRTPoly(PrivateKeyDCRTPoly&&) = delete;
    PrivateKeyDCRTPoly& operator=(const PrivateKeyDCRTPoly&) = delete;
    PrivateKeyDCRTPoly& operator=(PrivateKeyDCRTPoly&&) = delete;

    [[nodiscard]] std::shared_ptr<PrivateKeyImpl> GetInternal() const;
};

// Generator functions
[[nodiscard]] std::unique_ptr<PrivateKeyDCRTPoly> GenNullPrivateKey();

} // openfhe
